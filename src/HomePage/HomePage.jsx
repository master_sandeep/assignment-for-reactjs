import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout() {
        this.props.logout();
    }

    render() {
        const { user } = this.props;
        return (
            <div >
                <h1>Home Page</h1>
                <table className="table table-dark">
                    <tbody>
                        <tr>
                            <td>
                                <Link to="/edit-profile" className="btn btn-link">Edit Profile</Link>
                                <Link to="" onClick={this.handleLogout} className="btn btn-link">Logout</Link>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Full Name</th>
                            <td>{user.fullName}</td>
                        </tr>
                        <tr>
                            <th scope="row">Contact Number</th>
                            <td>{user.contactNumber}</td>
                        </tr>
                        <tr>
                            <th scope="row">Email</th>
                            <td>{user.email}</td>
                        </tr>
                        <tr>
                            <th scope="row">User Name</th>
                            <td>{user.username}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

function mapState(state) {
    const { authentication } = state;
    const { user } = authentication;
    return { user };
}

const actionCreators = {
    logout: userActions.logout
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };