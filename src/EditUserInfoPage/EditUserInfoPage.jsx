import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

class EditUserInfoPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: props.user,
            errors: {
                fullName: '',
                contactNumber: '',
                email: ''
            },
            isValid: true
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        let { user, errors } = this.state;
        errors = this.formValidator(name, value, errors)
        this.setState({
            user: {
                ...user,
                [name]: value,
            },
            errors: errors
        });
    }

    formValidator(name, value, errors) {
        switch (name) {
            case 'fullName':
                errors.fullName = value.length == 0 ? 'Full Name is required' :
                    value.length < 5
                        ? 'Full Name must be 5 characters long!'
                        : '';
                break;
            case 'email':
                errors.email = value.length == 0 ? 'Email is required' :
                    validEmailRegex.test(value)
                        ? ''
                        : 'Email is not valid!';
                break;
            case 'contactNumber':
                errors.contactNumber = value.length == 0 ? 'Contact number is required' :
                    value.match(/^(\+\d{1,3}[- ]?)?\d{10}$/)
                        ? ''
                        : 'Phone number is not valid';
                break;
            default:
                break;
        }

        return errors;
    }

    handleSubmit(event) {
        event.preventDefault();
        const { user, errors } = this.state;

        const isValid = this.validateForm()
        if (isValid) {
            this.setState({ isValid: true });
            this.props.update(user);
        } else {
            this.setState({ isValid: false });
        }
    }

    validateForm() {
        let valid = true;
        let { user, errors } = this.state;
        Object.keys(user).forEach((key) => {
            errors = this.formValidator(key, user[key], errors);
        })
        Object.values(errors).forEach(
            // if we have an error string set valid to false
            (val) => val.length > 0 && (valid = false)
        );
        this.setState({ errors: errors })
        return valid;
    }

    render() {
        const { isUpdateUserInfo } = this.props;
        const { user, errors, isValid } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>Edit Profile</h2>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (!isValid && errors.fullName.length > 0 ? ' has-error' : '')}>
                        <label htmlFor="fullName">Full Name <span className='text-danger'>*</span></label>
                        <input type="text" className="form-control" name="fullName" value={user.fullName} onChange={this.handleChange} />
                        {!isValid && errors.fullName.length > 0 &&
                            <div className="help-block">{errors.fullName}</div>
                        }
                    </div>
                    <div className={'form-group' + (!isValid && errors.contactNumber.length > 0 ? ' has-error' : '')}>
                        <label htmlFor="contactNumber">Contact Namber <span className='text-danger'>*</span></label>
                        <input type="text" className="form-control" name="contactNumber" value={user.contactNumber} onChange={this.handleChange} />
                        {!isValid && errors.contactNumber.length > 0 &&
                            <div className="help-block">{errors.contactNumber}</div>
                        }
                    </div>
                    <div className={'form-group' + (!isValid && errors.email.length > 0 ? ' has-error' : '')}>
                        <label htmlFor="email">Email <span className='text-danger'>*</span></label>
                        <input type="email" className="form-control" name="email" value={user.email} onChange={this.handleChange} />
                        {!isValid && errors.email.length > 0 &&
                            <div className="help-block">{errors.email}</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary">
                            {!isUpdateUserInfo &&
                                'Update'}
                            {isUpdateUserInfo &&
                                <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                            }
                        </button>
                        <Link to="/" className="btn btn-link">Cancel</Link>
                    </div>
                </form>
            </div>
        );
    }
}

function mapState(state) {
    const { authentication } = state;
    const { user, isUpdateUserInfo } = authentication;
    return { user, isUpdateUserInfo };
}

const actionCreators = {
    update: userActions.update
}

const connectedEditUserInfoPage = connect(mapState, actionCreators)(EditUserInfoPage);
export { connectedEditUserInfoPage as EditUserInfoPage };